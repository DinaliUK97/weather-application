package main

import (
	"encoding/json"
	"fmt"
	"github.com/labstack/echo"
	"io/ioutil"
	"net/http"
)

func weatherRoutes(c echo.Context) error {
	city_name := c.FormValue("city_name")
	//r:= new(Response)
	//err := new(error)
	var response Response
	url := "http://api.openweathermap.org/data/2.5/weather?q="
	query := url + city_name + "&appid=928f6c8ce318c1cdf910a87e88972b6b"
	fmt.Println(query)

	r, err := http.Get(query)

	if err != nil {
		return c.JSON(http.StatusCreated, err)
	}
	defer r.Body.Close()
	body, err:= ioutil.ReadAll(r.Body)
	if err != nil {
		return c.JSON(http.StatusCreated, err)
	}
	err = json.Unmarshal(body, &response)
	if err != nil {
		return c.JSON(http.StatusCreated, err)
	}
	return c.JSON(http.StatusCreated, response)

}
