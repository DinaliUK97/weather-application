package main

import "github.com/labstack/echo"

func main() {
	e := echo.New()

	e.GET("/",weatherRoutes )
	e.Logger.Fatal(e.Start(":3000"))

}
