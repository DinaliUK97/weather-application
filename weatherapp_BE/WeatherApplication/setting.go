package main

type Coord struct {
	Longitude float32 `json:"lon"`
	Latitude  float32 `json:"lat"`
}

type Weather struct {
	Id int `json:"id"`
	Main string `json:"main"`
	Description string `json:"description"`
	Icon string `json:"icon"`
}

type Main struct {
	Temperature float32 `json:"temp"`
	Pressure int `json:"pressure"`
	Humidity int `json:"humidity"`
	TemperatureMin float32 `json:"temp_min"`
	TemperatureMax float32 `json:"temp_max"`
}



type Wind struct {
	Speed float32 `json:"speed"`
	Degrees int `json:"deg"`
}

type Clouds struct {
	All int
}

type Sys struct {
	Type int `json:"type"`
	Id int `json:"id"`
	Message float32 `json:"message"`
	Country string `json:"country"`
	Sunrise int `json:"sunrise"`
	Sunset int `json:"sunset"`
}

type Response struct {
	Coord Coord
	Weather []Weather
	Base string
	Main Main
	Visibility int
	Wind Wind
	Clouds Clouds
	Dt int
	Sys Sys
	Timezone int
	Id int
	Name string
	Cod int

}
