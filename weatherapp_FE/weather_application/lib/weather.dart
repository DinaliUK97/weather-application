import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class TextInputValue extends StatefulWidget {
  _TextInputValueState createState() => _TextInputValueState();
}

class _TextInputValueState extends State<TextInputValue> {
  TextEditingController _textInputController = TextEditingController();
  String _showText = "";
  String display= "";

  Future<String> _onPressed() async{
    setState(() {
      display = "Loading...";
    });
    var response = await http.get("http://192.168.8.161:3000/?city_name=${_showText}");
    setState(() {
      display = "";
    });

    if(response.statusCode==201){
      Map<String,dynamic> json_data = json.decode(response.body);
      String msg = "\nTemperature max : ${json_data['Main']['temp_max']}k\n"
          "\nTemperature min : ${json_data['Main']['temp_min']}k\n"
          "\nPressure :${json_data['Main']['pressure']}pa\n"
          "\nHumidity :${json_data['Main']['humidity']}%\n"
          "\nWind Speed :${json_data['Wind']['speed']}Km/hr\n "
  ;
      print(json_data);
      setState(() {
        display = msg;
      });

    }
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        backgroundColor: Colors.deepPurpleAccent,

        appBar: AppBar(

          title: Text('Weather Application'),
        ),
        body: Center(
        child: Container(

    constraints: BoxConstraints.expand(),
    decoration: BoxDecoration(
    image: DecorationImage(
    image: NetworkImage("https://2.bp.blogspot.com/-nGB9GO_zJpg/XldO3ykoWVI/AAAAAAAAjnE/EpdXclYe13ob5VvITbolailqBQuBfq3TQCLcBGAsYHQ/s1600/9333580b4ab7471dd3e4ba0f6c82c9fc.jpg"),
    fit: BoxFit.cover)
    ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[

              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  style: TextStyle(color: Colors.white),
                  controller: _textInputController,
                  autocorrect: true,
                  onChanged: (val){
                    setState(() {
                      _showText = val;
                    });
                  },
                  decoration:
                      InputDecoration(hintText: 'Enter a City name here',
                        icon: Icon(Icons.search,color: Colors.white,)
                        ,
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(
                              color: Colors.white
                          )
                      ),)
                  ,

                ),
              ),
              RaisedButton(
                onPressed: this._showText.length>=2?_onPressed:null,
                textColor: Colors.white,
                color: Colors.blue,

                child: Text('Submit'),
              ),
              Text((display),textAlign: TextAlign.justify,
                  overflow: TextOverflow.fade,
                  style: TextStyle(fontWeight: FontWeight.bold,color: Colors.white)),
            ],
          ),
        )));
  }
}